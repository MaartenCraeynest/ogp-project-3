package tests;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.Calendar;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import filesystem.*;
import filesystem.exception.*;

public class ShortCutTest {
	Directory rootDir, subDirectoryWritable, subDirectoryNotWritable, dirTerminated, tempWritableDir;
	
	File fileWritableDirWritable, fileTerminated;
	
	ShortCut terminatedShortCut,shortCutDirWritable,shortCutDirNotWritable,testShortCutDirectoryStringFileLegalCase,
				shortCutTerminatedDir, shortCutNotWritableDir, shortCutIllegalName, shortCutNullParent, shortCutRefTerminated,shortCutNullReference,
				testShortCutDirectoryStringDirLegalCase,shortCutFile,shortCutDir;
	
	//Inherited method variables
	ShortCut shortCutTerminateTest, ShortCutcanAcceptAsNewNameTest, shortCutTestChangeName, shortCutOrderTest, shortCutCreationTimeTest,
				shortCutModificationTimeTest, shortCutOverlappingUsePeriodTest, shortCutNotModified, shortCutModified, shortCutGetRootTest,
					shortCutMoveTest, shortCutCanHaveAsParentDirectoryTest,shortCutIsDirectOrIndirectParentOfTest;
	
	@Before
	public void setUp() throws Exception {
		rootDir = new Directory("rootDir");
		// Temporarily writable directory that will be made not writable after setup
		tempWritableDir = new Directory("tempWritableDir", true);
		subDirectoryWritable = new Directory(rootDir, "subDirWritable", true);
		subDirectoryNotWritable = new Directory(rootDir, "subDirNotWritable", false);
		
		fileWritableDirWritable		= new File(subDirectoryWritable, 	"fileWritableDirWritable",		Type.JAVA, 0, true);

		dirTerminated = new Directory("TerminatedDir");
		dirTerminated.terminate();
		
		fileTerminated = new File(rootDir,"TerminatedFile",Type.JAVA);
		fileTerminated.terminate();
		
		terminatedShortCut 	= new ShortCut(rootDir, "ternminatedShortcut", fileWritableDirWritable);
		terminatedShortCut.terminate();
		
		shortCutDirWritable = new ShortCut(subDirectoryWritable, "shortCutDirWritable", fileWritableDirWritable);
		
		shortCutDirNotWritable = new ShortCut(tempWritableDir, "shortCutDirNotWritable", fileWritableDirWritable);
		
		//Make the tempWritableDir not writable
		tempWritableDir.setWritable(false);
		
		shortCutFile = new ShortCut(rootDir, "shortCutFile", fileWritableDirWritable);
		shortCutDir = new ShortCut(rootDir, "shortCutDir", rootDir);
		
		//StartInherited method variables
		shortCutTerminateTest = new ShortCut(rootDir, "TerminateTest", rootDir);
		ShortCutcanAcceptAsNewNameTest = new ShortCut(rootDir, "ShortCutcanAcceptAsNewNameTest", rootDir);
		shortCutTestChangeName = new ShortCut(rootDir, "shortCutTestChangeName", rootDir);
		shortCutOrderTest = new ShortCut(rootDir, "m", rootDir);
		shortCutCreationTimeTest = new ShortCut(rootDir, "shortCutCreationTimeTest", rootDir);
		shortCutModificationTimeTest = new ShortCut(rootDir,"shortCutModificationTimeTest",rootDir);
		shortCutOverlappingUsePeriodTest = new ShortCut(rootDir, "shortCutOverlappingUsePeriodTest", rootDir);
		shortCutNotModified = new ShortCut(rootDir,"shortCutNotModified", rootDir);
		shortCutModified = new ShortCut(rootDir,"shortCutModified", rootDir);
		shortCutModified.changeName("shortCutModifiedNewName");
		shortCutGetRootTest = new ShortCut(rootDir, "shortCutGetRoodTest", rootDir);
		shortCutMoveTest = new ShortCut(rootDir, "shortCutMoveTest", rootDir);
		shortCutCanHaveAsParentDirectoryTest = new ShortCut(rootDir, "shortCutCanHaveAsParentDirectoryTest", rootDir);
		shortCutIsDirectOrIndirectParentOfTest = new ShortCut(rootDir, "shortCutIsDirectOrIndirectParentOfTest", rootDir);
	}

	@After
	public void tearDown() throws Exception {
	}

	//Start Constructor Tests
	@Test
	public void testShortCutDirectoryStringFileLegalCase() {
		testShortCutDirectoryStringFileLegalCase = new ShortCut(subDirectoryWritable, "testShortCutDirectoryStringFileLegalCase",fileWritableDirWritable);
		assertEquals(testShortCutDirectoryStringFileLegalCase.getName(), "testShortCutDirectoryStringFileLegalCase");
		assertEquals(testShortCutDirectoryStringFileLegalCase.getParentDirectory(), subDirectoryWritable);
		assertEquals(testShortCutDirectoryStringFileLegalCase.getReference(), fileWritableDirWritable);
		assertFalse(testShortCutDirectoryStringFileLegalCase.isTerminated());
		assertNull(testShortCutDirectoryStringFileLegalCase.getModificationTime());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testShortCutDirectoryStringFileTerminatedDirectory() {
		shortCutTerminatedDir = new ShortCut(dirTerminated, "shortCutTerminatedDir", fileWritableDirWritable);
	}
	
	@Test (expected = EntryNotWritableException.class)
	public void testShortCutDirectoryStringFileNotWritableParent() {
		shortCutNotWritableDir = new ShortCut(subDirectoryNotWritable, "shortCutNotWritableDir", fileWritableDirWritable);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testShortCutDirectoryStringFileIllegalName() {
		shortCutIllegalName = new ShortCut(subDirectoryWritable, "%�*/.?", fileWritableDirWritable);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testShortCutDirectoryStringFileParentNull() {
		shortCutNullParent = new ShortCut(null, "shortCutNullParent", fileWritableDirWritable);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testShortCutDirectoryStringFileTerminatedReference() {
		shortCutRefTerminated = new ShortCut(subDirectoryWritable, "shortCutNullParent", fileTerminated);
	}
	
	@Test
	public void testShortCutDirectoryStringDirLegalCase() {
		testShortCutDirectoryStringDirLegalCase = new ShortCut(subDirectoryWritable, "testShortCutDirectoryStringDirLegalCase",subDirectoryWritable);
		assertEquals(testShortCutDirectoryStringDirLegalCase.getName(), "testShortCutDirectoryStringDirLegalCase");
		assertEquals(testShortCutDirectoryStringDirLegalCase.getParentDirectory(), subDirectoryWritable);
		assertEquals(testShortCutDirectoryStringDirLegalCase.getReference(), subDirectoryWritable);
		assertFalse(testShortCutDirectoryStringDirLegalCase.isTerminated());
		assertNull(testShortCutDirectoryStringDirLegalCase.getModificationTime());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testShortCutDirectoryStringDirTerminatedDirectory() {
		shortCutTerminatedDir = new ShortCut(dirTerminated, "shortCutTerminatedDir", subDirectoryWritable);
	}
	
	@Test (expected = EntryNotWritableException.class)
	public void testShortCutDirectoryStringDirNotWritableParent() {
		shortCutNotWritableDir = new ShortCut(subDirectoryNotWritable, "shortCutNotWritableDir", subDirectoryWritable);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testShortCutDirectoryStringDirIllegalName() {
		shortCutIllegalName = new ShortCut(subDirectoryWritable, "%�*/.?", subDirectoryWritable);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testShortCutDirectoryStringDirParentNull() {
		shortCutNullParent = new ShortCut(null, "shortCutNullParent", subDirectoryWritable);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testShortCutDirectoryStringDirTerminatedReference() {
		shortCutRefTerminated = new ShortCut(subDirectoryWritable, "shortCutNullParent", dirTerminated);
	}
	
	@Test
	public void testIsValidName() {
		assertTrue(shortCutFile.isValidName("validnameaA0_.-"));
		assertFalse(shortCutFile.isValidName("invalidname%"));
		assertTrue(shortCutDir.isValidName("validnameaA0_-"));
		assertFalse(shortCutDir.isValidName("."));
		assertFalse(shortCutDir.isValidName("invalidname%"));
	}
	
	@Test
	public void testCanHaveAsParentDirectory() {
		assertFalse(shortCutCanHaveAsParentDirectoryTest.canHaveAsParentDirectory(dirTerminated));
		assertFalse(terminatedShortCut.canHaveAsParentDirectory(subDirectoryWritable));
		assertFalse(shortCutCanHaveAsParentDirectoryTest.canHaveAsParentDirectory(subDirectoryNotWritable));
		assertTrue(shortCutCanHaveAsParentDirectoryTest.canHaveAsParentDirectory(null));
		assertTrue(shortCutCanHaveAsParentDirectoryTest.canHaveAsParentDirectory(shortCutCanHaveAsParentDirectoryTest.getParentDirectory()));
		ShortCut temp = new ShortCut(subDirectoryWritable, "shortCutCanHaveAsParentDirectoryTest", rootDir);
		assertFalse(shortCutCanHaveAsParentDirectoryTest.canHaveAsParentDirectory(subDirectoryWritable));
		temp.changeName("tempName");
		assertTrue(shortCutCanHaveAsParentDirectoryTest.canHaveAsParentDirectory(subDirectoryWritable));
	}

	// start inherited methods 
	// TODO When copying check that overrides are not forgotten
	@Test
	public void testCanBeTerminated() {
		assertFalse(terminatedShortCut.canBeTerminated());
		assertTrue(shortCutDirWritable.canBeTerminated());
		assertFalse(shortCutDirNotWritable.canBeTerminated());
	}
	
	@Test
	public void testTerminateLegal() {
		shortCutTerminateTest.terminate();
		assertTrue(shortCutTerminateTest.isTerminated());
		assertTrue(shortCutTerminateTest.getParentDirectory()== null);
	}
	
	@Test (expected = IllegalStateException.class)
	public void testTerminateNotWritable() {
		shortCutTerminateTest.getParentDirectory().setWritable(false);
		shortCutTerminateTest.terminate();
	}
	
	@Test
	public void testCanAcceptAsNewName() {
		assertFalse(ShortCutcanAcceptAsNewNameTest.canAcceptAsNewName(ShortCutcanAcceptAsNewNameTest.getName()));
		assertTrue(ShortCutcanAcceptAsNewNameTest.canAcceptAsNewName("testname"));

		assertFalse(ShortCutcanAcceptAsNewNameTest.canAcceptAsNewName("TerminateTest"));
	
		ShortCutcanAcceptAsNewNameTest.terminate();
		assertFalse(ShortCutcanAcceptAsNewNameTest.canAcceptAsNewName("testname"));
	}
	

	@Test
	public void testChangeName() {
		File comparefile = new File(shortCutTestChangeName.getParentDirectory(), "aName",Type.JAVA);
		shortCutTestChangeName.changeName("aaanewname");
		assertFalse(shortCutTestChangeName.getModificationTime()== null);
		assertEquals(shortCutTestChangeName.getName(), "aaanewname");
		assertTrue(shortCutTestChangeName.isOrderedBefore(comparefile.getName()));
	}
	
	@Test (expected = IllegalStateException.class)
	public void testChangeNameTerminated() {
		terminatedShortCut.changeName("newname");
	}
	
	@Test
	public void testChangeNameAlreadyExists() {
		shortCutFile.changeName("shortCutDir");
		assertEquals(shortCutFile.getModificationTime(), null);
		assertEquals(shortCutFile.getName(), "shortCutFile");
	}
	
	@Test
	public void testIsOrderedAfterString() {
		//ShortCutOrderTest has name "m"
		assertFalse(shortCutOrderTest.isOrderedAfter("n"));
		assertTrue(shortCutOrderTest.isOrderedAfter("l"));
		assertFalse(shortCutOrderTest.isOrderedAfter("M"));
		assertFalse(shortCutOrderTest.isOrderedAfter("m"));
		assertTrue(shortCutOrderTest.isOrderedAfter("7"));
		assertTrue(shortCutOrderTest.isOrderedAfter("."));
	}

	@Test
	public void testIsOrderedBeforeString() {
		//ShortCutOrderTest has name "m"
		assertTrue(shortCutOrderTest.isOrderedBefore("n"));
		assertFalse(shortCutOrderTest.isOrderedBefore("l"));
		assertFalse(shortCutOrderTest.isOrderedBefore("M"));
		assertFalse(shortCutOrderTest.isOrderedBefore("m"));
		assertFalse(shortCutOrderTest.isOrderedBefore("7"));
		assertFalse(shortCutOrderTest.isOrderedBefore("."));
	}

	@Test
	public void testIsOrderedAfterDiskItem() {
		assertFalse(shortCutOrderTest.isOrderedAfter(new ShortCut(rootDir, "n", rootDir)));
		assertTrue(shortCutOrderTest.isOrderedAfter(new ShortCut(rootDir, "l", rootDir)));
		assertTrue(shortCutOrderTest.isOrderedAfter(new ShortCut(rootDir, "7", rootDir)));
		assertTrue(shortCutOrderTest.isOrderedAfter(new ShortCut(rootDir, "-", rootDir)));
	}

	@Test
	public void testIsOrderedBeforeDiskItem() {
		assertTrue(shortCutOrderTest.isOrderedBefore(new ShortCut(rootDir, "n", rootDir)));
		assertFalse(shortCutOrderTest.isOrderedBefore(new ShortCut(rootDir, "l", rootDir)));
		assertFalse(shortCutOrderTest.isOrderedBefore(new ShortCut(rootDir, "7", rootDir)));
		assertFalse(shortCutOrderTest.isOrderedBefore(new ShortCut(rootDir, "-", rootDir)));
	}

	@Test
	public void testIsValidCreationTime() {
		assertFalse(ShortCut.isValidCreationTime(null));
	}

	@Test
	public void testCanHaveAsModificationTime() {
		Date creationTime = (Date) shortCutModificationTimeTest.getCreationTime();
		Calendar cal = Calendar.getInstance();
		cal.setTime(creationTime); 
		cal.add(Calendar.MONTH, 1); 
		
		assertTrue(shortCutModificationTimeTest.canHaveAsModificationTime(creationTime));
		assertFalse(shortCutModificationTimeTest.canHaveAsModificationTime(cal.getTime()));
		cal.add(Calendar.MONTH, -2); 
		assertFalse(shortCutModificationTimeTest.canHaveAsModificationTime(cal.getTime()));
	}

	@Test
	public void testHasOverlappingUsePeriod() {
		assertFalse(shortCutOverlappingUsePeriodTest.hasOverlappingUsePeriod(null));
		assertFalse(shortCutNotModified.hasOverlappingUsePeriod(shortCutModified));
		assertFalse(shortCutModified.hasOverlappingUsePeriod(shortCutNotModified));
		assertTrue(shortCutModified.hasOverlappingUsePeriod(shortCutModified));
	}

	@Test
	public void testGetRoot() {
		assertEquals(shortCutGetRootTest.getRoot(), rootDir);
	}

	@Test (expected = IllegalStateException.class)
	public void testMoveTerminated() {
		shortCutMoveTest.terminate();
		shortCutMoveTest.move(rootDir);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testMoveNullTarget() {
		shortCutMoveTest.move(null);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testMoveTargetParent() {
		shortCutMoveTest.move(shortCutMoveTest.getParentDirectory());
	}

	@Test (expected = IllegalArgumentException.class)
	public void testMoveParentCantHaveAsItem() {
		Directory dir = new Directory(shortCutMoveTest.getParentDirectory(), "tempDir");
		new ShortCut(dir, "shortCutMoveTest", dir);
		shortCutMoveTest.move(dir);
	}
	
	@Test (expected = EntryNotWritableException.class)
	public void testMoveParentNotWritable() {
		shortCutMoveTest.move(tempWritableDir);
	}
	
	@Test 
	public void testMoveLegal() {
		shortCutMoveTest.move(subDirectoryWritable);
		assertFalse(rootDir.hasAsItem(shortCutMoveTest));
		assertEquals(shortCutMoveTest.getParentDirectory(), subDirectoryWritable);
		assertTrue(subDirectoryWritable.hasAsItem(shortCutMoveTest));
	}

	@Test
	public void testIsDirectOrIndirectParentOf() {
		assertFalse(shortCutIsDirectOrIndirectParentOfTest.isDirectOrIndirectParentOf(null));
		assertFalse(shortCutIsDirectOrIndirectParentOfTest.isDirectOrIndirectParentOf(shortCutIsDirectOrIndirectParentOfTest));
		assertFalse(shortCutIsDirectOrIndirectParentOfTest.isDirectOrIndirectParentOf(subDirectoryWritable));
	}


	@Test
	public void testGetAbsolutePath() {
		assertEquals(shortCutFile.getAbsolutePath(), "/rootDir/shortCutFile");
	}

}
