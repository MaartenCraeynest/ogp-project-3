package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import filesystem.Directory;
import filesystem.DirectoryIterator;
import filesystem.Entry;
import filesystem.File;
import filesystem.ShortCut;
import filesystem.Type;
import filesystem.exception.*;
import org.junit.Before;
import org.junit.Test;

public class DirectoryTest {
	Directory rootDir,dirWritableEmpty, dirNotWritableEmpty,dirWritableFull, dirNotWritableFull, dirTerminated, dirParentNotWritable, dirParentWritable,
				testDirectoryStringBoolean, testDirectoryString, testDirectoryDirectoryStringBoolean, testDirectoryDirectoryString, dirDefaultName, 
				dirCanHaveAsItemTest, dirGetTotalSizeTest, dirDeleteRecursiveTest, dirDeleteRecursiveTestSubDirectory, dirDeleteRecursiveSubSubDirectory,
				dirChangeNameTest, dirOrderTest, dirModificationTimeTest,dirModified, dirNotModified;
	File fileDeleteRecursiveTest;	
	ShortCut shortCutDeleteRecursiveTest;
	
	
	@Before
	public void setUp(){
		rootDir = new Directory("rootDir");
		
		dirWritableEmpty = new Directory(rootDir, "dirWritableEmpty", true);
		dirNotWritableEmpty = new Directory(rootDir, "dirNotWritableEmpty", false);
		
		dirWritableFull = new Directory(rootDir, "dirWritableFull", true);
		dirParentWritable = new Directory(dirWritableFull, "dirParentWritable", true);
		
		dirNotWritableFull = new Directory(rootDir, "dirNotWritableFull", true);
		dirParentNotWritable = new Directory(dirNotWritableFull, "dirParentNotWritable", true);
		dirNotWritableFull.setWritable(false);
		
		dirTerminated = new Directory(rootDir, "dirTerminated", true);
		dirTerminated.terminate();
		
		dirDefaultName = new Directory(rootDir, "    ", true);
		dirCanHaveAsItemTest = new Directory("dirCanHaveAsItemTest");
		dirChangeNameTest = new Directory(rootDir, "dirChangeNameTest");
		
		dirOrderTest = new Directory(rootDir, "m");
		dirModificationTimeTest = new Directory(rootDir, "dirModificationTimeTest");
		dirNotModified = new Directory(rootDir, "dirNotModified");
		dirModified = new Directory(rootDir, "dirModified");
		dirModified.changeName("modifiedname");
	}

	@Test
	public void testCanBeTerminated() {
		assertFalse(dirTerminated.canBeTerminated());
		assertTrue(dirWritableEmpty.canBeTerminated());
		assertFalse(dirNotWritableEmpty.canBeTerminated());
		assertFalse(dirWritableFull.canBeTerminated());
		assertFalse(dirParentNotWritable.canBeTerminated());
	}

	@Test
	public void testTerminateLegal() {
		dirWritableEmpty.terminate();
		assertTrue(dirWritableEmpty.isTerminated());
		assertTrue(dirWritableEmpty.isRoot());
	}
	
	@Test (expected = IllegalStateException.class)
	public void testTerminateParentNotWritable() {
		dirParentNotWritable.terminate();
	}
	
	@Test (expected = EntryNotWritableException.class)
	public void testTerminateNotWritable() {
		dirNotWritableEmpty.terminate();
	}
	
	@Test (expected = IllegalStateException.class)
	public void testTerminateFull() {
		dirWritableFull.terminate();
	}

	@Test
	public void testIsValidName() {
		assertFalse(dirWritableEmpty.isValidName(null));
		assertTrue(dirWritableEmpty.isValidName("aA0_-"));
		assertFalse(dirWritableEmpty.isValidName("."));
		assertFalse(dirWritableEmpty.isValidName(" "));
	}

	@Test (expected = EntryNotWritableException.class)
	public void testMakeRootNotWritable() {
		dirNotWritableEmpty.makeRoot();
	}
	
	@Test (expected = EntryNotWritableException.class)
	public void testMakeRootDirNotWritable() {
		dirNotWritableEmpty.makeRoot();
	}
	
	@Test (expected = EntryNotWritableException.class)
	public void testMakeRootParentNotWritable() {
		dirParentNotWritable.makeRoot();
	}
	
	@Test (expected = IllegalStateException.class)
	public void testMakeRootTerminated() {
		dirTerminated.makeRoot();
	}

	@Test
	public void testDirectoryDirectoryStringBoolean() {
		testDirectoryDirectoryStringBoolean = new Directory(rootDir, "testDirectoryDirectoryStringBoolean", true);
		assertEquals(rootDir, testDirectoryDirectoryStringBoolean.getParentDirectory());
		assertEquals("testDirectoryDirectoryStringBoolean", testDirectoryDirectoryStringBoolean.getName());
		assertTrue(testDirectoryDirectoryStringBoolean.isWritable());
		assertEquals(testDirectoryDirectoryStringBoolean.getNbItems(), 0);
	}
	
	@Test (expected = EntryNotWritableException.class)
	public void testDirectoryDirectoryStringBooleanParentNotWritable() {
		testDirectoryDirectoryStringBoolean = new Directory(dirNotWritableEmpty, "testDirectoryDirectoryStringBoolean", true);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testDirectoryDirectoryStringBooleanDefaultNameExists() {
		testDirectoryDirectoryStringBoolean = new Directory(rootDir, "   ", true);
	}
	
	@Test 
	public void testDirectoryDirectoryStringBooleanDefaultName() {
		testDirectoryDirectoryStringBoolean = new Directory(dirWritableFull, "IllegalName  ", true);
		assertEquals(dirWritableFull, testDirectoryDirectoryStringBoolean.getParentDirectory());
		assertEquals("new_diskitem", testDirectoryDirectoryStringBoolean.getName());
		assertTrue(testDirectoryDirectoryStringBoolean.isWritable());
		assertEquals(testDirectoryDirectoryStringBoolean.getNbItems(), 0);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testDirectoryDirectoryStringBooleanNullParent() {
		testDirectoryDirectoryStringBoolean = new Directory(null, "dirWritableEmpty", true);
	}

	@Test
	public void testDirectoryDirectoryString() {
		testDirectoryDirectoryString = new Directory(rootDir, "testDirectoryDirectoryString");
		assertEquals(rootDir, testDirectoryDirectoryString.getParentDirectory());
		assertEquals("testDirectoryDirectoryString", testDirectoryDirectoryString.getName());
		assertTrue(testDirectoryDirectoryString.isWritable());
		assertEquals(testDirectoryDirectoryString.getNbItems(), 0);
	}
	
	@Test (expected = EntryNotWritableException.class)
	public void testDirectoryDirectoryStringParentNotWritable() {
		testDirectoryDirectoryString = new Directory(dirNotWritableEmpty, "testDirectoryDirectoryStringBoolean");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testDirectoryDirectoryStringDefaultNameExists() {
		testDirectoryDirectoryString = new Directory(rootDir, "   ");
	}
	
	@Test 
	public void testDirectoryDirectoryStringDefaultName() {
		testDirectoryDirectoryString = new Directory(dirWritableFull, "IllegalName  ");
		assertEquals(dirWritableFull, testDirectoryDirectoryString.getParentDirectory());
		assertEquals("new_diskitem", testDirectoryDirectoryString.getName());
		assertTrue(testDirectoryDirectoryString.isWritable());
		assertEquals(testDirectoryDirectoryString.getNbItems(), 0);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testDirectoryDirectoryStringNullParent() {
		testDirectoryDirectoryString = new Directory(null, "dirWritableEmpty");
	}

	@Test
	public void testDirectoryString() {
		testDirectoryString = new Directory("testDirectoryString");
		assertEquals(null, testDirectoryString.getParentDirectory());
		assertEquals("testDirectoryString", testDirectoryString.getName());
		assertTrue(testDirectoryString.isWritable());
		assertEquals(testDirectoryString.getNbItems(), 0);
	}
	
	public void testDirectoryStringBoolean() {
		testDirectoryStringBoolean = new Directory("testDirectoryStringBoolean", false);
		assertEquals(null, testDirectoryStringBoolean.getParentDirectory());
		assertEquals("testDirectoryStringBoolean", testDirectoryStringBoolean.getName());
		assertFalse(testDirectoryStringBoolean.isWritable());
		assertEquals(testDirectoryStringBoolean.getNbItems(), 0);
	}

	@Test (expected = IndexOutOfBoundsException.class)
	public void testGetItemAtminus1() {
		dirWritableFull.getItemAt(-1);
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void testGetItemAt0() {
		dirWritableFull.getItemAt(0);
	}
	
	@Test
	public void testGetItemAt1() {
		assertEquals(dirWritableFull.getItemAt(1), dirParentWritable);
	}

	@Test
	public void testCanHaveAsItem() {
		assertFalse(dirTerminated.canHaveAsItem(dirWritableEmpty));
		assertFalse(dirWritableEmpty.canHaveAsItem(dirTerminated));
		assertFalse(dirWritableEmpty.canHaveAsItem(null));
		assertTrue(dirCanHaveAsItemTest.canHaveAsItem(rootDir));
		assertTrue(dirCanHaveAsItemTest.canHaveAsItem(dirWritableEmpty));
		assertFalse(dirParentWritable.canHaveAsItem(dirWritableFull));
		assertTrue(dirWritableFull.canHaveAsItem(dirParentWritable));
		assertFalse(dirWritableFull.canHaveAsItem(new Directory("dirParentWritable")));
	}

	@Test
	public void testCanHaveAsItemAt() {
		assertFalse(dirTerminated.canHaveAsItemAt(dirWritableEmpty, 0));
		assertFalse(dirWritableEmpty.canHaveAsItemAt(dirTerminated, 0));
		assertFalse(dirWritableEmpty.canHaveAsItemAt(null, 0));
		assertFalse(dirParentWritable.canHaveAsItemAt(dirWritableFull, 0));
		assertFalse(dirWritableFull.canHaveAsItemAt(new Directory("dirParentWritable"), 0));
		assertFalse(rootDir.canHaveAsItemAt(dirWritableEmpty, -1));
		assertFalse(rootDir.canHaveAsItemAt(dirWritableEmpty, 0));
		assertFalse(rootDir.canHaveAsItemAt(dirWritableEmpty, rootDir.getNbItems()+2));
		
		Directory tempDir= new Directory(rootDir, "tempDir");
		Directory b = new Directory(tempDir, "b");
		Directory x = new Directory(tempDir, "y");
		b.setWritable(false);
		x.setWritable(false);
		
		assertTrue(tempDir.canHaveAsItemAt(new Directory("a"), 1));
		assertFalse(tempDir.canHaveAsItemAt(new Directory("a"), 2));
		assertTrue(tempDir.canHaveAsItemAt(new Directory("c"), 2));
		assertFalse(tempDir.canHaveAsItemAt(new Directory("c"), 1));
		assertFalse(tempDir.canHaveAsItemAt(new Directory("c"), 3));
		assertFalse(tempDir.canHaveAsItemAt(new Directory("z"), 2));
		assertTrue(tempDir.canHaveAsItemAt(new Directory("z"), 3));
		
		Directory a = new Directory(tempDir, "a");
		Directory c = new Directory(tempDir, "c");
		Directory z = new Directory(tempDir, "z");
		a.setWritable(false);
		c.setWritable(false);
		z.setWritable(false);
		assertTrue(tempDir.canHaveAsItemAt(a, 1));
		assertFalse(tempDir.canHaveAsItemAt(a, 2));
		assertTrue(tempDir.canHaveAsItemAt(c, 3));
		assertFalse(tempDir.canHaveAsItemAt(c, 2));
		assertFalse(tempDir.canHaveAsItemAt(c, 4));
		assertFalse(tempDir.canHaveAsItemAt(z, 4));
		assertTrue(tempDir.canHaveAsItemAt(z, 5));
	}

	@Test
	public void testHasProperItems() {
		assertTrue(dirWritableEmpty.hasProperItems());
		assertTrue(dirWritableFull.hasProperItems());
	}

	@Test
	public void testHasAsItem() {
		assertTrue(dirWritableFull.hasAsItem(dirParentWritable));
		assertFalse(dirWritableFull.hasAsItem(rootDir));
		assertFalse(rootDir.hasAsItem(null));
	}

	@Test
	public void testIsDirectOrIndirectSubdirectoryOf() {
		assertTrue(rootDir.isDirectOrIndirectParentOf(dirWritableEmpty));
		assertTrue(rootDir.isDirectOrIndirectParentOf(dirParentWritable));
		assertFalse(dirParentWritable.isDirectOrIndirectParentOf(rootDir));
		assertFalse(dirWritableEmpty.isDirectOrIndirectParentOf(dirWritableFull));
	}

	@Test
	public void testContainsDiskItemWithName() {
		assertTrue(rootDir.containsDiskItemWithName("DirWritableEmpty"));
		assertFalse(rootDir.containsDiskItemWithName("superuniquenamethatnobodywilleveruse"));
	}

	@Test
	public void testExists() {
		assertTrue(dirWritableFull.exists("dirParentWritable"));
		assertFalse(dirWritableFull.exists("rootDir"));
		assertTrue(dirWritableFull.exists("dirparentwritable"));
	}

	@Test
	public void testGetItem() {
		assertEquals(dirWritableFull.getItem("rootDir"), null);
		assertEquals(dirWritableFull.getItem("dirParentWritable"), dirParentWritable);
		assertEquals(dirWritableFull.getItem("dirWritableFull"), null);
	}

	@Test
	public void testGetIndexOfLegal() {
		assertEquals(dirWritableFull.getIndexOf(dirParentWritable), 1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testGetIndexOfNonExistant() {
		dirWritableFull.getIndexOf(rootDir);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testGetIndexOfNull() {
		dirWritableFull.getIndexOf(null);
	}

	@Test
	public void testIterator() {
		DirectoryIterator it = rootDir.Iterator();
		assertEquals(it.getNbRemainingItems(), rootDir.getNbItems());
		for (int i = 1; i < rootDir.getNbItems(); i++ ){
			assertEquals(it.getCurrentItem(), rootDir.getItemAt(i));
			it.advance();
		}	
	}

	@Test
	public void testGetTotalSize() {
		dirGetTotalSizeTest = new Directory("dirGetTotalSizetest");
		assertEquals(dirGetTotalSizeTest.getTotalSize(), 0);
		Directory dirSubDirectory = new Directory(dirGetTotalSizeTest,"dirSubDirectory");
		assertEquals(dirGetTotalSizeTest.getTotalSize(),0);
		new ShortCut(dirGetTotalSizeTest, "shortcut", dirGetTotalSizeTest);
		assertEquals(dirGetTotalSizeTest.getTotalSize(),0);
		new ShortCut(dirSubDirectory, "shortcut", dirSubDirectory);
		assertEquals(dirGetTotalSizeTest.getTotalSize(),0);
		
		new File(dirGetTotalSizeTest, "emptypdffile", Type.PDF);
		assertEquals(dirGetTotalSizeTest.getTotalSize(),0);
		new File(dirGetTotalSizeTest, "fullpdffile", Type.PDF,10, true);
		assertEquals(dirGetTotalSizeTest.getTotalSize(),10);
		new File(dirSubDirectory, "emptypdffile", Type.PDF, 10,true);
		assertEquals(dirGetTotalSizeTest.getTotalSize(),20);
	}

	@Test
	public void testDeleteRecursiveLegal() {
		dirDeleteRecursiveTest = new Directory("dirDeleteRecursiveTest");
		dirDeleteRecursiveTestSubDirectory = new Directory(dirDeleteRecursiveTest,"dirDeleteRecusiveTestSubDirectory");
		dirDeleteRecursiveSubSubDirectory = new Directory(dirDeleteRecursiveTestSubDirectory,"dirDeleteRecusiveTestSubSubDirectory");
		fileDeleteRecursiveTest = new File(dirDeleteRecursiveSubSubDirectory, "fileDeleteRecursiveTest", Type.PDF, 10, true);
		shortCutDeleteRecursiveTest = new ShortCut(dirDeleteRecursiveTestSubDirectory, "shortCutDeleteRecursiveTest", rootDir);
		
		dirDeleteRecursiveTest.deleteRecursive();
		
		assertTrue(dirDeleteRecursiveTest.isTerminated());
		assertTrue(dirDeleteRecursiveTestSubDirectory.isTerminated());
		assertTrue(dirDeleteRecursiveSubSubDirectory.isTerminated());
		assertTrue(fileDeleteRecursiveTest.isTerminated());
		assertTrue(shortCutDeleteRecursiveTest.isTerminated());
		
		assertEquals(dirDeleteRecursiveTest.getParentDirectory(), null);
		assertEquals(dirDeleteRecursiveTestSubDirectory.getParentDirectory(), null);
		assertEquals(dirDeleteRecursiveSubSubDirectory.getParentDirectory(), null);
		assertEquals(fileDeleteRecursiveTest.getParentDirectory(), null);
		assertEquals(shortCutDeleteRecursiveTest.getParentDirectory(), null);
	}
	
	@Test (expected =EntryNotWritableException.class)
	public void testDeleteRecursiveNonWritableFile() {
		dirDeleteRecursiveTest = new Directory("dirDeleteRecursiveTest");
		dirDeleteRecursiveTestSubDirectory = new Directory(dirDeleteRecursiveTest,"dirDeleteRecusiveTestSubDirectory");
		dirDeleteRecursiveSubSubDirectory = new Directory(dirDeleteRecursiveTestSubDirectory,"dirDeleteRecusiveTestSubSubDirectory");
		fileDeleteRecursiveTest = new File(dirDeleteRecursiveSubSubDirectory, "fileDeleteRecursiveTest", Type.PDF, 10, false);
		shortCutDeleteRecursiveTest = new ShortCut(dirDeleteRecursiveTestSubDirectory, "shortCutDeleteRecursiveTest", rootDir);
		
		dirDeleteRecursiveTest.deleteRecursive();
	}
	
	@Test (expected =EntryNotWritableException.class)
	public void testDeleteRecursiveNonWritableDirectory() {
		dirDeleteRecursiveTest = new Directory("dirDeleteRecursiveTest");
		dirDeleteRecursiveTestSubDirectory = new Directory(dirDeleteRecursiveTest,"dirDeleteRecusiveTestSubDirectory");
		dirDeleteRecursiveSubSubDirectory = new Directory(dirDeleteRecursiveTestSubDirectory,"dirDeleteRecusiveTestSubSubDirectory");
		fileDeleteRecursiveTest = new File(dirDeleteRecursiveSubSubDirectory, "fileDeleteRecursiveTest", Type.PDF, 10, true);
		shortCutDeleteRecursiveTest = new ShortCut(dirDeleteRecursiveTestSubDirectory, "shortCutDeleteRecursiveTest", rootDir);
		dirDeleteRecursiveSubSubDirectory.setWritable(false);
		dirDeleteRecursiveTest.deleteRecursive();
	}
	
	@Test (expected = IllegalStateException.class)
	public void testDeleteRecursiveTerminated(){
		dirTerminated.deleteRecursive();
	}

	@Test
	public void testFindNonWritableEntries() {
		dirDeleteRecursiveTest = new Directory("dirDeleteRecursiveTest");
		dirDeleteRecursiveTestSubDirectory = new Directory(dirDeleteRecursiveTest,"dirDeleteRecusiveTestSubDirectory");
		dirDeleteRecursiveSubSubDirectory = new Directory(dirDeleteRecursiveTestSubDirectory,"dirDeleteRecusiveTestSubSubDirectory");
		fileDeleteRecursiveTest = new File(dirDeleteRecursiveSubSubDirectory, "fileDeleteRecursiveTest", Type.PDF, 10, false);
		shortCutDeleteRecursiveTest = new ShortCut(dirDeleteRecursiveTestSubDirectory, "shortCutDeleteRecursiveTest", rootDir);
		dirDeleteRecursiveSubSubDirectory.setWritable(false);
		ArrayList<Entry> nonWritable =  dirDeleteRecursiveTest.findNonWritableEntries();
		assertEquals(nonWritable.size(), 2);
		assertEquals(nonWritable.get(0), dirDeleteRecursiveSubSubDirectory);
		assertEquals(nonWritable.get(1), fileDeleteRecursiveTest);
	}

	@Test
	public void testCanAcceptAsNewName() {
		assertFalse(dirParentWritable.canAcceptAsNewName(dirParentWritable.getName()));
		assertTrue(dirParentWritable.canAcceptAsNewName("testname"));
		
		assertFalse(dirWritableEmpty.canAcceptAsNewName("dirWritableFull"));
		assertFalse(dirNotWritableEmpty.canAcceptAsNewName("testname"));
		assertFalse(dirTerminated.canAcceptAsNewName("testname"));
	}

	@Test
	public void testChangeName() {
		assertFalse(dirChangeNameTest.isOrderedBefore(dirCanHaveAsItemTest.getName()));
		dirChangeNameTest.changeName("aaanewname");
		assertFalse(dirChangeNameTest.getModificationTime()== null);
		assertEquals(dirChangeNameTest.getName(), "aaanewname");
		assertTrue(dirChangeNameTest.isOrderedBefore(dirCanHaveAsItemTest.getName()));
	}
	
	@Test (expected = IllegalStateException.class)
	public void testChangeNameTerminated() {
		dirTerminated.changeName("newname");
	}
	
	@Test (expected = EntryNotWritableException.class)
	public void testChangeNameNotWritable(){
		dirNotWritableEmpty.changeName("newname");
	}
	
	@Test
	public void testChangeNameAllreadyExists() {
		assertFalse(dirChangeNameTest.isOrderedBefore(dirCanHaveAsItemTest.getName()));
		dirChangeNameTest.changeName("dirWritableEmpty");
		assertEquals(dirChangeNameTest.getModificationTime(), null);
		assertEquals(dirChangeNameTest.getName(), "dirChangeNameTest");
		assertFalse(dirChangeNameTest.isOrderedBefore(dirCanHaveAsItemTest.getName()));
	}

	@Test (expected = IllegalStateException.class)
	public void testMoveTerminated() {
		dirTerminated.move(rootDir);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testMoveNullTarget() {
		dirWritableEmpty.move(null);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testMoveTargetParent() {
		dirWritableEmpty.move(dirWritableEmpty.getParentDirectory());
	}

	@Test (expected = IllegalArgumentException.class)
	public void testMoveParentCantHaveAsItem() {
		Directory dir = new Directory(dirWritableEmpty.getParentDirectory(), "tempDir");
		new ShortCut(dir, "dirWritableEmpty", dir);
		dirWritableEmpty.move(dir);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testMoveParentNotWritable() {
		dirParentNotWritable.move(rootDir);
	}
	
	@Test 
	public void testMoveLegal() {
		dirParentWritable.move(dirWritableEmpty);
		assertFalse(dirWritableFull.hasAsItem(dirParentWritable));
		assertEquals(dirParentWritable.getParentDirectory(), dirWritableEmpty);
		assertTrue(dirWritableEmpty.hasAsItem(dirParentWritable));
	}

	@Test (expected = EntryNotWritableException.class)
	public void testMoveNotWritable() {
		dirNotWritableEmpty.move(dirWritableEmpty);
	}

	@Test
	public void testIsOrderedAfterString() {
		//ShortCutOrderTest has name "m"
		assertFalse(dirOrderTest.isOrderedAfter("n"));
		assertTrue(dirOrderTest.isOrderedAfter("l"));
		assertFalse(dirOrderTest.isOrderedAfter("M"));
		assertFalse(dirOrderTest.isOrderedAfter("m"));
		assertTrue(dirOrderTest.isOrderedAfter("7"));
		assertTrue(dirOrderTest.isOrderedAfter("."));
	}

	@Test
	public void testIsOrderedBeforeString() {
		//ShortCutOrderTest has name "m"
		assertTrue(dirOrderTest.isOrderedBefore("n"));
		assertFalse(dirOrderTest.isOrderedBefore("l"));
		assertFalse(dirOrderTest.isOrderedBefore("M"));
		assertFalse(dirOrderTest.isOrderedBefore("m"));
		assertFalse(dirOrderTest.isOrderedBefore("7"));
		assertFalse(dirOrderTest.isOrderedBefore("."));
	}

	@Test
	public void testIsOrderedAfterDiskItem() {
		assertFalse(dirOrderTest.isOrderedAfter(new ShortCut(rootDir, "n", rootDir)));
		assertTrue(dirOrderTest.isOrderedAfter(new ShortCut(rootDir, "l", rootDir)));
		assertTrue(dirOrderTest.isOrderedAfter(new ShortCut(rootDir, "7", rootDir)));
		assertTrue(dirOrderTest.isOrderedAfter(new ShortCut(rootDir, "-", rootDir)));
	}

	@Test
	public void testIsOrderedBeforeDiskItem() {
		assertTrue(dirOrderTest.isOrderedBefore(new ShortCut(rootDir, "n", rootDir)));
		assertFalse(dirOrderTest.isOrderedBefore(new ShortCut(rootDir, "l", rootDir)));
		assertFalse(dirOrderTest.isOrderedBefore(new ShortCut(rootDir, "7", rootDir)));
		assertFalse(dirOrderTest.isOrderedBefore(new ShortCut(rootDir, "-", rootDir)));
	}

	@Test
	public void testIsValidCreationTime() {
		assertFalse(Directory.isValidCreationTime(null));
	}

	@Test
	public void testCanHaveAsModificationTime() {
		Date creationTime = (Date) dirModificationTimeTest.getCreationTime();
		Calendar cal = Calendar.getInstance();
		cal.setTime(creationTime); 
		cal.add(Calendar.MONTH, 1); 
		
		assertTrue(dirModificationTimeTest.canHaveAsModificationTime(creationTime));
		assertFalse(dirModificationTimeTest.canHaveAsModificationTime(cal.getTime()));
		cal.add(Calendar.MONTH, -2); 
		assertFalse(dirModificationTimeTest.canHaveAsModificationTime(cal.getTime()));
	}

	@Test
	public void testHasOverlappingUsePeriod() {
		assertFalse(dirModified.hasOverlappingUsePeriod(null));
		assertFalse(dirNotModified.hasOverlappingUsePeriod(dirModified));
		assertFalse(dirModified.hasOverlappingUsePeriod(dirNotModified));
		assertTrue(dirModified.hasOverlappingUsePeriod(dirModified));
	}

	@Test
	public void testGetRoot() {
		assertEquals(dirParentNotWritable.getRoot(), rootDir);
	}

	@Test
	public void testCanHaveAsParentDirectory() {
		assertFalse(dirWritableEmpty.canHaveAsParentDirectory(dirTerminated));
		assertFalse(dirTerminated.canHaveAsParentDirectory(dirWritableEmpty));
		assertFalse(dirWritableEmpty.canHaveAsParentDirectory(dirNotWritableEmpty));
		assertTrue(dirWritableEmpty.canHaveAsParentDirectory(null));
		assertTrue(dirWritableEmpty.canHaveAsParentDirectory(dirWritableEmpty.getParentDirectory()));
		ShortCut temp = new ShortCut(dirWritableFull, "dirWritableEmpty", rootDir);
		assertFalse(dirWritableEmpty.canHaveAsParentDirectory(dirWritableFull));
		temp.changeName("tempName");
		assertTrue(dirWritableEmpty.canHaveAsParentDirectory(dirWritableFull));
	}

	@Test
	public void testIsDirectOrIndirectParentOf() {
		assertFalse(rootDir.isDirectOrIndirectParentOf(null));
		assertFalse(dirNotWritableFull.isDirectOrIndirectParentOf(dirWritableEmpty));
		assertTrue(dirNotWritableFull.isDirectOrIndirectParentOf(dirParentNotWritable));
		assertTrue(rootDir.isDirectOrIndirectParentOf(dirParentNotWritable));
	}

	@Test
	public void testGetAbsolutePath() {
		assertEquals(dirParentNotWritable.getAbsolutePath(), "/rootDir/dirNotWritableFull/dirParentNotWritable");
	}

}
