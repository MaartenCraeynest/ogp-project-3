package tests;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import filesystem.Directory;
import filesystem.File;
import filesystem.Type;
import filesystem.exception.EntryNotWritableException;

public class FileTest {
	
	 File fileDirectoryStringIntBooleanString;
	 File fileDirectoryStringString;
	 Date timeBeforeConstruction, timeAfterConstruction;
	 Directory directoryString;
	 Directory directoryNotWritable;
	 Directory subdirectory;
	 Type pdf;
	 Type txt;
	
	 File fileNotWritable;
	 File fileTerminated;
	 Directory directoryTerminated;
	 Date timeBeforeConstructionNotWritable, timeAfterConstructionNotWritable;
	 

	@Before
	public void setUp() throws Exception {
		pdf=Type.PDF;
		txt=Type.TEXT;
		timeBeforeConstruction = new Date();
		directoryString=new Directory("dir");
		directoryNotWritable=new Directory("notWritable",false);
		subdirectory=new Directory(directoryString,"subdir",true);
		fileDirectoryStringIntBooleanString=new File(directoryString,"bestand",txt,100,true);
		fileDirectoryStringString=new File(directoryString,"bestand2",txt);
		timeAfterConstruction = new Date();

		timeBeforeConstructionNotWritable = new Date();
		fileNotWritable = new File(directoryString,"bestand3",txt,100,false);
		fileTerminated=new File(directoryString,"Untitled",txt,100,true);
		directoryTerminated=new Directory("terminated");
		timeAfterConstructionNotWritable = new Date();
		fileTerminated.terminate();
		directoryTerminated.terminate();
	}

	@Test
	public void testCanBeTerminated() {
		assertTrue(fileDirectoryStringString.canBeTerminated());
		assertFalse(fileTerminated.canBeTerminated());
		assertFalse(fileNotWritable.canBeTerminated());
		directoryString.setWritable(false);
		assertFalse(fileDirectoryStringString.canBeTerminated());
	}

	@Test
	public void testGetAbsolutePath() {
		assertEquals(fileDirectoryStringString.getAbsolutePath(),"/dir/bestand2.txt");
		assertEquals(fileNotWritable.getAbsolutePath(),"/dir/bestand3.txt");
		assertEquals(directoryString.getAbsolutePath(),"/dir");
		assertEquals(subdirectory.getAbsolutePath(),"/dir/subdir");
		File f=new File(subdirectory,"test",pdf);
		assertEquals(f.getAbsolutePath(),"/dir/subdir/test.pdf");
	}

	@Test
	public void testFileDirectoryStringTypeIntBoolean() {
		assertEquals("bestand",fileDirectoryStringIntBooleanString.getName());
		assertEquals(fileDirectoryStringIntBooleanString.getSize(),100);
		assertTrue(fileDirectoryStringIntBooleanString.isWritable());
		assertNull(fileDirectoryStringIntBooleanString.getModificationTime());
		assertFalse(timeBeforeConstruction.after(fileDirectoryStringIntBooleanString.getCreationTime()));
		assertFalse(fileDirectoryStringIntBooleanString.getCreationTime().after(timeAfterConstruction));
		assertEquals(fileDirectoryStringIntBooleanString.getType(),txt);
		assertEquals(fileDirectoryStringIntBooleanString.getParentDirectory(),directoryString);
		assertFalse(fileNotWritable.isWritable());
	}
	@Test (expected = IllegalArgumentException.class)
	public void testFileDirectoryStringTypeIntBoolean_IllegalCase() throws IllegalArgumentException{
		File f=new File(null,"Untitled",txt,100,true);
		assert false;
		f.getClass(); 
	}

	@Test
	public void testFileDirectoryStringType() {
		assertEquals("bestand2",fileDirectoryStringString.getName());
		assertEquals(fileDirectoryStringString.getSize(),0);
		assertTrue(fileDirectoryStringString.isWritable());
		assertNull(fileDirectoryStringString.getModificationTime());
		assertFalse(timeBeforeConstruction.after(fileDirectoryStringString.getCreationTime()));
		assertFalse(fileDirectoryStringString.getCreationTime().after(timeAfterConstruction));
		assertEquals(fileDirectoryStringString.getType(),txt);
		assertEquals(fileDirectoryStringString.getParentDirectory(),directoryString);
	}
	@Test (expected = IllegalArgumentException.class)
	public void testFileDirectoryStringType_IllegalCase() throws IllegalArgumentException{
		File f=new File(directoryString,"bestand2",txt);
		f.isWritable(); //just to prevent the variablenotused warning
	}

	@Test
	public void testIsValidType() {
		assertFalse(File.isValidType(null));
		assertTrue(File.isValidType(pdf));
	}

	@Test
	public void testGetType() {
		assertEquals(fileNotWritable.getType(),txt);
		assertFalse(fileDirectoryStringString.getType().equals(pdf));
	}

	@Test
	public void testGetMaximumSize() {
		assertEquals(File.getMaximumSize(),Integer.MAX_VALUE);
	}

	@Test
	public void testIsValidSize() {
		assertFalse(File.isValidSize(Integer.MAX_VALUE+1));
		assertFalse(File.isValidSize(-50));
		assertTrue(File.isValidSize(10000));
	}

	@Test
	public void testEnlarge() {
		File file = new File(directoryString,"bestand4",txt,File.getMaximumSize()-1,true);
		Date timeBeforeEnlarge = new Date();
		file.enlarge(1);
		Date timeAfterEnlarge = new Date();		
		assertEquals(file.getSize(),File.getMaximumSize());
		assertNotNull(file.getModificationTime());
		assertFalse(file.getModificationTime().before(timeBeforeEnlarge));
		assertFalse(timeAfterEnlarge.before(file.getModificationTime())); 
	}
	@Test (expected = EntryNotWritableException.class)
	public void testEnlarge_FileNotWritable() throws EntryNotWritableException{
		fileNotWritable.enlarge(1);
	}
	@Test
	public void testShorten() throws EntryNotWritableException{
		fileDirectoryStringIntBooleanString.shorten(1);
		Date timeAfterShorten = new Date();		
		assertEquals(fileDirectoryStringIntBooleanString.getSize(),99);
		assertNotNull(fileDirectoryStringIntBooleanString.getModificationTime());
		assertFalse(fileDirectoryStringIntBooleanString.getModificationTime().before(timeAfterConstruction));
		assertFalse(timeAfterShorten.before(fileDirectoryStringIntBooleanString.getModificationTime()));
	}
	
	@Test (expected = EntryNotWritableException.class)
	public void testShorten_FileNotWritable() throws EntryNotWritableException{
		fileNotWritable.shorten(1);
	}

	@Test
	public void testTerminate() {
		File fTerminate=new File(directoryString,"terminate",txt);
		fTerminate.terminate();
		assertTrue(fTerminate.isTerminated());
	}
	@Test (expected=EntryNotWritableException.class)
	public void testTerminate_FileNotWritable()throws EntryNotWritableException{
		fileNotWritable.terminate();
	}
	@Test (expected=IllegalStateException.class)
	public void testTerminate_FileNotTerminatable()throws IllegalStateException{
			File file=new File(subdirectory,"notTerminatable",txt);
			subdirectory.setWritable(false);
			file.terminate();
	}

	@Test
	public void testCanAcceptAsNewName() {
		assertFalse(fileDirectoryStringString.canAcceptAsNewName("bestand"));
		assertFalse(fileDirectoryStringString.canAcceptAsNewName("bestand2"));
		assertTrue(fileDirectoryStringString.canAcceptAsNewName("test"));
		assertFalse(fileDirectoryStringString.canAcceptAsNewName("bestand@�"));
		fileDirectoryStringString.terminate();
		assertFalse(fileDirectoryStringString.canAcceptAsNewName("test1"));
	}

	@Test
	public void testChangeName() {
		assertTrue(fileDirectoryStringIntBooleanString.isOrderedBefore(fileDirectoryStringString));
		fileDirectoryStringString.changeName("a");
		assertTrue(fileDirectoryStringString.getModificationTime().getTime()<(new Date()).getTime()+100 && fileDirectoryStringString.getModificationTime().getTime()>(new Date()).getTime()-100);
		assertFalse(fileDirectoryStringIntBooleanString.isOrderedBefore(fileDirectoryStringString));
		fileDirectoryStringString.changeName("&");
		assertFalse(fileDirectoryStringString.getName().equals("&"));
	}
	@Test (expected=IllegalStateException.class)
	public void testChangeName_FileTerminated()throws IllegalStateException{
			fileTerminated.changeName("test");
	}

	@Test
	public void testMove() {
		File fileA=new File(subdirectory,"a",txt);
		File fileB=new File(subdirectory,"ba",txt);
		fileA.move(directoryString);
		fileB.move(directoryString);
		assertFalse(subdirectory.exists(fileA.getName()));
		assertEquals(fileDirectoryStringString.getParentDirectory(),directoryString);
		assertTrue(directoryString.hasAsItem(fileDirectoryStringString));
		fileDirectoryStringString.move(subdirectory);
		assertEquals(fileDirectoryStringString.getParentDirectory(),subdirectory);
		assertTrue(directoryString.hasAsItem(fileA));
		assertTrue(directoryString.hasAsItem(fileB));
		assertTrue((directoryString.getIndexOf(fileA)) < (directoryString.getIndexOf(fileB)));
	}
	@Test (expected=IllegalStateException.class)
	public void testMove_FileTerminated()throws IllegalStateException{
		fileTerminated.move(directoryString);
	}
	@Test (expected=EntryNotWritableException.class)
	public void testMove_DirectoryNotWritable()throws EntryNotWritableException{
		fileDirectoryStringString.move(directoryNotWritable);
	}
	@Test (expected=IllegalArgumentException.class)
	public void testMove_CannotHaveDirectory()throws IllegalArgumentException{
		fileDirectoryStringString.move(directoryString);
	}
	@Test (expected=IllegalArgumentException.class)
	public void testMove_CannotHaveNull()throws IllegalArgumentException{
		fileDirectoryStringString.move(null);
	}
	@Test
	public void testMakeRoot() {
		Directory dir=new Directory(directoryString,"dir2");
		assertFalse(dir.getRoot().equals(dir));
		dir.makeRoot();
		assertTrue(dir.getModificationTime().getTime()<(new Date()).getTime()+1 && dir.getModificationTime().getTime()>(new Date()).getTime()-1);
		assertTrue(dir.getRoot().equals(dir));
		assertFalse(directoryString.hasAsItem(dir));
	}
	@Test (expected=IllegalStateException.class)
	public void testMakeRoot_FileTerminated()throws IllegalStateException{
		directoryTerminated.makeRoot();
	}
	@Test (expected=EntryNotWritableException.class)
	public void testMakeRoot_FileNotWritable()throws EntryNotWritableException{
		Directory superdir=new Directory("superdir");
		Directory dir=new Directory(superdir,"directory");
		superdir.setWritable(false);
		dir.makeRoot();
	}

	@Test
	public void testCanHaveAsParentDirectory() {
		assertFalse(fileDirectoryStringString.canHaveAsParentDirectory(directoryTerminated));
		assertFalse(fileTerminated.canHaveAsParentDirectory(subdirectory));
		assertTrue(fileTerminated.canHaveAsParentDirectory(null));
		directoryString.setWritable(false);
		assertFalse(fileDirectoryStringString.canHaveAsParentDirectory(null));
	}

	@Test
	public void testIsWritable() {
		assertTrue(fileDirectoryStringString.isWritable());
		directoryString.setWritable(false);
		assertTrue(fileDirectoryStringString.isWritable());
		assertFalse(directoryString.isWritable());
	}

	@Test
	public void testSetWritable() {
		fileDirectoryStringString.setWritable(true);
		assertTrue(fileDirectoryStringString.isWritable());
		fileDirectoryStringString.setWritable(false);
		assertFalse(fileDirectoryStringString.isWritable());
	}

	@Test
	public void testIsTerminated() {
		assertFalse(fileDirectoryStringString.isTerminated());
		fileDirectoryStringString.terminate();
		assertTrue(fileDirectoryStringString.isTerminated());
	}

	@Test
	public void testGetName() {
		assertEquals(fileDirectoryStringString.getName(),"bestand2");
	}

	@Test
	public void testIsValidName() {
		assertFalse(fileDirectoryStringString.isValidName(null));
		assertTrue(fileDirectoryStringString.isValidName("bestand3"));
		assertFalse(fileDirectoryStringString.isValidName("&�@"));
		assertTrue(fileNotWritable.isValidName("nAme1_9.5"));
	}

	@Test
	public void testIsOrderedAfterString() {
		assertTrue(fileNotWritable.isOrderedAfter("A"));
		assertFalse(fileNotWritable.isOrderedAfter("bestand3"));
		assertFalse(fileDirectoryStringString.isOrderedAfter("bestand3"));
	}

	@Test
	public void testIsOrderedBeforeString() {
		assertFalse(fileNotWritable.isOrderedBefore("A"));
		assertFalse(fileNotWritable.isOrderedBefore("bestand3"));
		assertTrue(fileDirectoryStringString.isOrderedBefore("bestand3"));
	}

	@Test
	public void testIsOrderedAfterDiskItem() {
		File f=new File(directoryString,"A",txt);
		assertTrue(fileNotWritable.isOrderedAfter(f));
		assertFalse(fileNotWritable.isOrderedAfter(fileNotWritable));
		assertFalse(fileDirectoryStringString.isOrderedAfter(fileNotWritable));
	}

	@Test
	public void testIsOrderedBeforeDiskItem() {
		File f=new File(directoryString,"A",txt);
		assertFalse(fileNotWritable.isOrderedBefore(f));
		assertFalse(fileNotWritable.isOrderedBefore(fileNotWritable));
		assertTrue(fileDirectoryStringString.isOrderedBefore(fileNotWritable));
	}

	@Test
	public void testGetCreationTime() {
		File f=new File(directoryString,"A",txt);
		assertTrue(f.getCreationTime().getTime()<=(new Date()).getTime()&& f.getCreationTime().getTime()>((new Date()).getTime()-10));
	}

	@Test
	public void testIsValidCreationTime() {
		assertTrue(File.isValidCreationTime(timeBeforeConstruction));
		assertFalse(File.isValidCreationTime((new Date(System.currentTimeMillis()+500))));
	}

	@Test
	public void testGetModificationTime() {
		assertTrue(fileDirectoryStringString.getModificationTime()==null);
		fileDirectoryStringString.changeName("bestand1");
		assertTrue(fileDirectoryStringString.getModificationTime().getTime()<=(new Date()).getTime()&& fileDirectoryStringString.getModificationTime().getTime()>((new Date()).getTime()-10));

	}

	@Test
	public void testCanHaveAsModificationTime() {
		assertTrue(fileNotWritable.canHaveAsModificationTime(fileNotWritable.getCreationTime()));
		assertFalse(fileNotWritable.canHaveAsModificationTime((new Date(System.currentTimeMillis()+500))));
		assertFalse(fileNotWritable.canHaveAsModificationTime((new Date(fileNotWritable.getCreationTime().getTime()-10))));
	}

	@Test
	public void testSetModificationTime() {
		fileDirectoryStringString.changeName("bestand1");
		assertTrue(fileDirectoryStringString.getModificationTime().getTime()<=System.currentTimeMillis()&& fileDirectoryStringString.getModificationTime().getTime()>System.currentTimeMillis()-10);
	}

	@Test
	public void testHasOverlappingUsePeriod() {
		assertFalse(fileDirectoryStringString.hasOverlappingUsePeriod(null));
		assertFalse(fileDirectoryStringString.hasOverlappingUsePeriod(fileDirectoryStringIntBooleanString));
		fileDirectoryStringString.changeName("bestand1");
		assertFalse(fileDirectoryStringString.hasOverlappingUsePeriod(fileDirectoryStringIntBooleanString));
		fileDirectoryStringIntBooleanString.changeName("bestand2");
		assertTrue(fileDirectoryStringString.hasOverlappingUsePeriod(fileDirectoryStringIntBooleanString));
		try {
		    Thread.sleep(1);                 
		} catch(InterruptedException ex) {
		    Thread.currentThread().interrupt();
		}
		File f=new File(directoryString,"test",txt);
		f.changeName("test1");
		assertFalse(fileDirectoryStringString.hasOverlappingUsePeriod(f));
		
	}

	@Test
	public void testGetRoot() {
		assertEquals(directoryString.getRoot(),directoryString);
		assertEquals(fileDirectoryStringString.getRoot(),directoryString);
		assertEquals(fileNotWritable.getRoot(),directoryString);
		assertEquals(subdirectory.getRoot(),directoryString);
	}

	@Test
	public void testIsDirectOrIndirectParentOf() {
		assertTrue(directoryString.isDirectOrIndirectParentOf(fileNotWritable));
		assertFalse(directoryString.isDirectOrIndirectParentOf(null));
		assertTrue(directoryString.isDirectOrIndirectParentOf(fileNotWritable));
	}

}
