
package filesystem.exception;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import filesystem.Entry;

/**
 * A class for signaling illegal attempts to change an entry
 * due to writability restrictions.
 */
public class EntryNotWritableException extends RuntimeException {

	/**
	 * Serialversion required by java
	 */
	private static final long serialVersionUID = 5764443045080909433L;

	/**
	 * Variable referencing the entry to which change was denied.
	 */
	private final Entry item;

	/**
	 * Initialize this new not writable exception involving the
	 * given entry.
	 * 
	 * @param	item
	 * 			The entry for the new not writable exception.
	 * @post	The entry involved in the new not writable exception
	 * 			is set to the given entry.
	 * 			| new.getItem() == item
	 */
	@Raw
	public EntryNotWritableException(Entry item) {
		this.item = item;
	}
	
	/**
	 * Return the entry involved in this not writable exception.
	 */
	@Raw @Basic
	public Entry getItem() {
		return item;
	}
	
}
