package filesystem;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Model;
import be.kuleuven.cs.som.annotate.Raw;
import filesystem.exception.EntryNotWritableException;

/**
 * Abstract class of all the items that have a writability parameter
 * @author Maarten Craeynest, Antony Dejonckheere
 * @version 1.0
 *
 */
public abstract class Entry extends DiskItem{
	/**
	 * Initialize a new root entry with given name and writability.
	 * 
	 * @param  	name
	 *         	The name of the new entry.
	 * @param  	writable
	 *         	The writability of the new entry.
	 * 
	 * @effect  The new Entry is a DiskItem with the given name
	 * 			| super(name)
	 * @effect	The writability is set to the given flag
	 * 			| setWritable(writable)
	 */
	@Model
	protected Entry(String name, boolean writable) {
		super(name);
		setWritable(writable);
	}

	/**
	 * Initialize a new entry with given parent directory, name and 
	 * writability.
	 *   
	 * @param  	parent
	 *         	The parent directory of the new entry.
	 * @param  	name
	 *         	The name of the new entry.  
	 * @param  	writable
	 *         	The writability of the new entry.
	 *         
	 * @effect  The new Entry is a DiskItem with the given name and
	 * 			parent. 
	 *          | super(parent, name) 
	 * @effect	The writability is set to the given flag
	 * 			| setWritable(writable)
	 */
	@Model
	protected Entry(Directory parent, String name, boolean writable)throws IllegalArgumentException, EntryNotWritableException{ 
		super(parent, name);
		setWritable(writable);
	}
	
	/**********************************************************
	 * writable
	 **********************************************************/

	/**
	 * Variable registering whether or not this entry is writable.
	 */
	private boolean isWritable = true;

	/**
	 * Check whether this entry is writable.
	 */
	@Raw @Basic
	public boolean isWritable() {
		return isWritable;
	}

	/**
	 * Set the writability of this entry to the given writability.
	 *
	 * @param isWritable
	 *        The new writability
	 * @post  The given writability is registered as the new writability
	 *        for this entry.
	 *        | new.isWritable() == isWritable
	 */
	@Raw 
	protected void setWritable(boolean isWritable) {
		this.isWritable = isWritable;
	}
	
	/**
	 * Check whether this entry can be terminated.
	 * 
	 * @return	True if the entry is not yet terminated, is writable and it is either a root or
	 * 			its parent directory is writable
	 * 			| if (isTerminated() || !isWritable() || (!isRoot() && !getParentDirectory().isWritable()))
	 * 			| then result == false
	 * @note	This specification must be left open s.t. the subclasses can change it
	 */
	@Override
	public boolean canBeTerminated(){
		return !isTerminated() && isWritable() && (isRoot() || getParentDirectory().isWritable());
	}
	
	/**
	 * Turns this entry into an root directory.
	 * 
	 * @effect	The Entry is made a root by the super function  
	 * 			| super.makeroot()
	 * @throws	EntryNotWritableException(this)
	 * 			This directory is not a root and it is not writable
	 * 			| !isRoot() && !isWritable()
	 * @note	Should only be used by directories or to terminate entries
	 */ 
	@Override
	protected void makeRoot() throws EntryNotWritableException,IllegalStateException {
		if (!isWritable()) 
			throw new EntryNotWritableException(this);
		super.makeRoot();
	}
	
	/**
	 * Check whether the name of this entry can be changed into the
	 * given name.
	 * 
	 * @return  True if this entry is not terminated, the given 
	 *          name is a valid name for an entry, this entry
	 *          is writable, the given name is different from the current name of this entry
	 *          and either this entry is a root item or the parent directory does not 
	 *          already contain an other item with the given name;
	 *          false otherwise.
	 *          | result == !isTerminated() && isWritable() && isValidName(name) && 
	 *          |			!getName().equals(name) && ( isRoot() || !getParentDirectory().exists(name) )
	 */
	@Override
	public boolean canAcceptAsNewName(String name) {
		return !isTerminated() && isWritable() && isValidName(name) && !getName().equals(name) &&
				(isRoot() || !getParentDirectory().exists(name));
	}
	
	/**
	 * Set the name of this entry to the given name.
	 *
	 * @param	name
	 * 			The new name for this entry.
	 * 
	 * @effect The change name method is called from the super function
	 * 			| super.changeName(name)
	 * @throws  EntryNotWritableException(this)
	 *          This entry is not writable.
	 *          | !isWritable()
	 */
	@Override
	public void changeName(String name) throws EntryNotWritableException, IllegalStateException {
		if (!isWritable()) 
			throw new EntryNotWritableException(this);
		super.changeName(name);
		
	}
	
	/**
	 * Move this entry to a given directory.
	 * 
	 * @param   target
	 *          The target directory.
	 *          
	 * @effect  The super function move is called.
	 * 			| super.move(target)
	 * @throws	EntryNotWritableException(this)
	 * 			This entry is not writable.
	 * 			| !isWritable()
	 */
	@Override
	public void move(Directory target) 
			throws IllegalArgumentException, EntryNotWritableException, IllegalStateException {
		if (!isWritable())
			throw new EntryNotWritableException(this);
		
		super.move(target);
		
	}
	
	/**
	 * Terminate this entry.
	 * 
	 * @effect 	The super terminate method is called.
	 * 			| super.terminate()
	 * @throws 	EntryNotWritableException
	 * 			if the entry is not writable
	 * 			|!isWritable()
	 * 		
	 */
	@Override
	public void terminate() throws IllegalStateException,EntryNotWritableException {
		if(!isWritable()){
			throw new EntryNotWritableException(this);
		}
		super.terminate();
	}
}
