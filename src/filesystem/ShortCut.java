package filesystem;

import be.kuleuven.cs.som.annotate.*;
import filesystem.exception.EntryNotWritableException;

/**
 * A class that allows references to other Entries.
 * 
 * @author Maarten Craeynest, Antony Dejonckheere
 * @version 1.0
 */
public class ShortCut extends DiskItem {
	/**
	 * Create a shortCut to a file with a given name in a directory.
	 * @param 	parent
	 * 			The directory where the shortcut will be located.
	 * @param 	name
	 * 			The name of the shortcut.
	 * @param 	file
	 * 			The file to reference to.
	 * 
	 * @effect	The shortcut is a DiskItem with the given name and in the 
	 * 			given directory.
	 * 			| super(parent, name)
	 * @post	The shortcut will refer to the given file
	 * 			| new.getReference() == file
	 * 
	 * @throws	IllegalArgumentException
	 * 			The file parameter is not in a valid state, is null or
	 * 			the name of the shortcut is invalid
	 *			| file.isTerminated() || File.isValidName(name) || file == null
	 */
	public ShortCut(Directory parent, String name, File file)
			throws IllegalArgumentException, EntryNotWritableException
	{
		super(parent, name);
		if (file.isTerminated() || !file.isValidName(name) || file == null)
			throw new IllegalArgumentException();
		
		this.reference = file;
	}
	
	/**
	 * Create a shortCut to a Directory with a given name in a directory.
	 * @param 	parent
	 * 			The directory where the shortcut will be located.
	 * @param 	name
	 * 			The name of the shortcut.
	 * @param 	directory
	 * 			The directory to reference to.
	 * 
	 * @effect	The shortcut is a DiskItem with the given name and in the 
	 * 			given directory.
	 * 			| super(parent, name)
	 * @post	The shortcut will refer to the given directory
	 * 			| new.getReference() == directory
	 * 
	 * @throws	IllegalArgumentException
	 * 			The directory parameter is not in a valid state, is null or
	 * 			the name of the shortcut does not follow the naming rules
	 * 			of directory
	 *			| directory.isTerminated() || Directory.isValidName(name) || directory == null
	 */
	public ShortCut(Directory parent, String name, Directory directory)
			throws IllegalArgumentException, EntryNotWritableException
	{
		super(parent, name);
		if (directory.isTerminated() || !directory.isValidName(name) || directory == null)
			throw new IllegalArgumentException();
		
		this.reference = directory;
	}
	
	/**********************************************************
	 * Reference
	 **********************************************************/
	
	/**
	 * Value to keep track of the object this link references to
	 */
	private final Entry reference;
	
	/**
	 * Returns the entry this link references to
	 */
	@Basic @Immutable
	public Entry getReference(){
		return this.reference;
	}
	
	/**
	 * Check whether this ShortCut can be terminated.
	 * 
	 * @return	True if the parent directory is writable and is not yet terminated, is writable and
	 * 			its parent directory is writable
	 * 			| result == getParentDirectory().isWritable() && !isTerminated()
	 * @note  	Now the specification is in closed form.
	 */
	
	@Override
	public boolean canBeTerminated(){
		return super.canBeTerminated();
	}
	
	/**
	 * Check whether the given name is a legal name for a Shortcut.
	 * 
	 * @param  	name
	 *			The name to be checked
	 * @return	If the referenced object is null then the result is the result of the 
	 * 			DiskItem's isvalidname. Else the result is the result of the isvalidname 
	 * 			method of the referenced object's class.
	 * 			|if (getReference() == null)
	 * 			|then result == (name != null && name.matches("[a-zA-Z_0-9.-]+"));
	 * 			|else if (getReference() instanceof File)
	 * 			|	  then	result == (File) getReference().isValidName(name) 
	 * 			|	  else if (getReference() instanceof Directory)
	 * 			|	  	   then result == (Directory) getReference().isValidName(name) 
	 */
	public boolean isValidName(String name) {
		if (getReference() == null) //Follow general naming convention of DiskItem
			return (name != null && name.matches("[a-zA-Z_0-9.-]+"));
		if (getReference() instanceof File)
			return ((File)getReference()).isValidName(name);
		else if (getReference() instanceof Directory)
			return ((Directory)getReference()).isValidName(name);
		else {
			//Should not happen
			assert false;
			return false;
		}
	}
	
	/** 
	 * Check whether this shortcut can have the given directory as
	 * its parent directory.
	 * 
	 * @param  	directory
	 *          The directory to check.
	 * @return  If this shortcut is terminated, 
	 * 			false if the directory is effective.
	 *          | if (this.isTerminated() && directory != null)
	 *          | then result == false
	 * @return	If this shortcut is not terminated,
	 * 				if the given directory is not effective,
	 * 				then false if the shortcut is not a root item and the parent is not writable
	 * 				else if the given directory is terminated, then false
	 * 					 if the parent directory of this shortcut is the same as the given directory,
	 * 					 then false if the given directory does not contains this shortcut
	 * 					 else false if the given directory is not writable or it can't have this item as an item
	 * 							or this item is not a root and the parent directory of this item is not writable
	 *			| if (!this.isTerminated())
	 *			| then if (directory == null)
	 *			|	   then result == (isRoot() || this.getParentDirectory().isWritable())
	 *			|	   else if (directory.isTerminated()) then result == false
	 *			|			if (this.getParentDirectory() == directory) 
	 *			|			then if (!directory.hasAsItem(this)) then result == false
	 *			|			else if   (!directory.isWritable() || !directory.canHaveAsItem(this) || (!this.isRoot() && !this.getParentDirectory().isWritable()) )
	 *			|				 then result == false
	 */
	@Raw @Override
	public boolean canHaveAsParentDirectory(Directory directory) {
		if (this.isTerminated())
			return (directory == null);
		if (directory == null)
			return (this.isRoot() || this.getParentDirectory().isWritable());
		if (directory.isTerminated())
			return false;
		if (getParentDirectory() == directory)
			return directory.hasAsItem(this);
		else return (directory.isWritable() && directory.canHaveAsItem(this) &&
				(this.isRoot() || this.getParentDirectory().isWritable()) );
	}
	
}
